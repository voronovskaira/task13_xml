import model.Gem;
import org.xml.sax.SAXException;
import parser.AbstractParser;
import parser.DomParser;
import parser.XmlValidator;
import parser.sax.SaxParser;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

public class Main {

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
       //AbstractParser parser = new DomParser();
        AbstractParser parser = new SaxParser();
        List<Gem> gems = parser.parseFile("src/main/resources/xml/gems.xml");
        gems.forEach(System.out::println);
        //System.out.println(XmlValidator.validateXMLSchema("src/main/resources/xml/gemsXSD.xsd",
           //     "src/main/resources/xml/gems.xml"));

    }

}

