package parser;

import model.Gem;
import model.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DomParser extends AbstractParser {
    private VisualParameters parameter;

    public List<Gem> parseFile(String fileName) {
        List<Gem> gemsList = new ArrayList<>();
        File fXmlFile = new File(fileName);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        Document doc = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(fXmlFile);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        NodeList gems = doc.getElementsByTagName("gem");
        for (int i = 0; i < gems.getLength(); i++) {
            Node gem = gems.item(i);
            Element gemElement = (Element) gem;
            Gem tempGem = new Gem();
            tempGem.setName(gemElement.getAttribute("name"));
            tempGem.setPreciousness(Boolean.valueOf(gemElement.getElementsByTagName("preciousness").item(0).getTextContent()));
            tempGem.setOrigin(gemElement.getElementsByTagName("origin").item(0).getTextContent());
            tempGem.setValue(Double.parseDouble(gemElement.getElementsByTagName("value").item(0).getTextContent()));
            tempGem.setVisualParameters(parseVisualParameter(gemElement.getElementsByTagName("visualParameters")));
            gemsList.add(tempGem);
        }
        return gemsList;
    }

    private VisualParameters parseVisualParameter(NodeList node) {
        for (int i = 0; i < node.getLength(); i++) {
            Node gem = node.item(i);
            Element gemElement = (Element) gem;
            parameter = new VisualParameters();
            parameter.setClarity(Integer.parseInt(gemElement.getElementsByTagName("clarity").item(0).getTextContent()));
            parameter.setColor(gemElement.getElementsByTagName("color").item(0).getTextContent());
            parameter.setFaces(Integer.parseInt(gemElement.getElementsByTagName("faces").item(0).getTextContent()));
        }
        return parameter;
    }
}
