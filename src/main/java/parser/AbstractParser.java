package parser;

import model.Gem;

import java.util.List;

public abstract class AbstractParser {
    public abstract List<Gem> parseFile(String filePath);
}
