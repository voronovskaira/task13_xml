package parser.sax;

import model.Gem;
import model.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SaxHandler extends DefaultHandler {
    static final String GEM_TAG = "gem";
    static final String PRECIOUSNESS_TAG = "preciousness";
    static final String ORIGIN_TAG = "origin";
    static final String VALUE_TAG = "value";
    static final String VISUAL_PARAMETERS_TAG = "visualParameters";
    static final String COLOR_TAG = "color";
    static final String CLARITY_TAG = "clarity";
    static final String FACES_TAG = "faces";

    static final String GEM_NAME_ATTRIBUTE = "name";

    List<Gem> gemsList = new ArrayList<>();
    private VisualParameters parameter;
    private String currentElement;
    private Gem gem;


    public List<Gem> getGemsList() {
        return gemsList;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentElement = qName;
        switch (qName) {
            case GEM_TAG: {
                gem = new Gem();
                gem.setName(attributes.getValue(GEM_NAME_ATTRIBUTE));
                break;
            }
            case VISUAL_PARAMETERS_TAG: {
                parameter = new VisualParameters();
                break;
            }
        }

    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case GEM_TAG: {
                gemsList.add(gem);
                break;
            }
            case VISUAL_PARAMETERS_TAG: {
                gem.setVisualParameters(parameter);
                parameter = null;
                break;
            }
        }
        currentElement = "empty";
    }

    public void characters(char[] ch, int start, int length) throws SAXException {

        if (currentElement.equals(PRECIOUSNESS_TAG)) {
            gem.setPreciousness(Boolean.parseBoolean(new String(ch, start, length)));
        }
        if (currentElement.equals(ORIGIN_TAG)) {
            gem.setOrigin(new String(ch, start, length));
        }
        if (currentElement.equals(VALUE_TAG)) {
            gem.setValue(Double.parseDouble(new String(ch, start, length)));
        }
        if (currentElement.equals(COLOR_TAG)) {
            parameter.setColor(new String(ch, start, length));
        }
        if (currentElement.equals(CLARITY_TAG)) {
            parameter.setClarity(Integer.parseInt(new String(ch, start, length)));
        }
        if (currentElement.equals(FACES_TAG)) {
            parameter.setFaces(Integer.parseInt(new String(ch, start, length)));
        }
    }

}