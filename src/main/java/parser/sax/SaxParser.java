package parser.sax;

import model.Gem;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import parser.AbstractParser;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SaxParser extends AbstractParser {
    private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

    @Override
    public List<Gem> parseFile(String filePath) {
        List<Gem> gems = new ArrayList<>();
        File xmlFile = new File(filePath);
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SaxHandler saxHandler = new SaxHandler();
            saxParser.parse(xmlFile, saxHandler);
            gems = saxHandler.getGemsList();
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            ex.printStackTrace();
        }
        return gems;

    }
}
